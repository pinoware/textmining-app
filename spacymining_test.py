#!/usr/bin/env python3

import spacy
from spacy import displacy

arr_lang = {}
arr_lang["es"] = "es_core_news_sm"
arr_lang["en"] = "en_core_web_sm"

nlp = spacy.load(arr_lang["es"]);
#doc = nlp(u'Apple is looking at buying U.K. startup for $1 billion')
doc = nlp(u'Apple está buscando comprar U.K. startup por mil millones de dolares.')

for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
          token.shape_, token.is_alpha, token.is_stop)

options = {'compact': True, 'color': 'blue'}
#options = {'ents': ['PERSON', 'ORG', 'PRODUCT'],
#           'colors': {'ORG': 'yellow'}}
displacy.serve(doc, style='dep', options=options)

#text = """But Google is starting from behind. The company made a late push
#into hardware, and Apple’s Siri, available on iPhones, and Amazon’s Alexa
#software, which runs on its Echo and Dot devices, have clear leads in
#consumer adoption."""

#nlp = spacy.load('custom_ner_model')
#doc = nlp(text)
#displacy.serve(doc, style='ent')