#!/usr/bin/env python3

import spacy

from textpipeliner import PipelineEngine, Context
from textpipeliner.pipes import *
from tkinter import *
from tkinter import messagebox

_root = Tk()

_txt_container = Text(_root)
_txt_container.pack()

arr_lang = {}
arr_lang["es"] = "es_core_news_sm"
arr_lang["en"] = "en_core_web_sm"

variable = StringVar(_root)
variable.set("es") # default value

w = OptionMenu(_root, variable, "es", "en")
w.pack(side='left')

#Text used for testing:
#The Empire of Japan aimed to dominate Asia and the Pacific and was already at war with the Republic of China in 1937, but the world war is generally said to have begun on 1 September 1939 with the invasion of Poland by Germany and subsequent declarations of war on Germany by France and the United Kingdom. From late 1939 to early 1941, in a series of campaigns and treaties, Germany conquered or controlled much of continental Europe, and formed the Axis alliance with Italy and Japan. Under the Molotov-Ribbentrop Pact of August 1939, Germany and the Soviet Union partitioned and annexed territories of their European neighbours, Poland, Finland, Romania and the Baltic states. The war continued primarily between the European Axis powers and the coalition of the United Kingdom and the British Commonwealth, with campaigns including the North Africa and East Africa campaigns, the aerial Battle of Britain, the Blitz bombing campaign, the Balkan Campaign as well as the long-running Battle of the Atlantic. In June 1941, the European Axis powers launched an invasion of the Soviet Union, opening the largest land theatre of war in history, which trapped the major part of the Axis' military forces into a war of attrition. In December 1941, Japan attacked the United States and European territories in the Pacific Ocean, and quickly conquered much of the Western Pacific.

def run_program():
      if not _txt_container.get("1.0", "end-1c"):
            messagebox.showerror("Vacío", "El campo de texto está vacío")
            return

      _lng_load = str(arr_lang[variable.get()])

      _nlp = spacy.load(_lng_load)
      _doc = _nlp(_txt_container.get("1.0", "end-1c"))

      _pipes_structure = [SequencePipe([FindTokensPipe("VERB/nsubj/*"),
                                    NamedEntityFilterPipe(),
                                    NamedEntityExtractorPipe()]),
                        FindTokensPipe("VERB"),
                        AnyPipe([SequencePipe([FindTokensPipe("VBD/dobj/NNP"),
                                                AggregatePipe([NamedEntityFilterPipe("GPE"), 
                                                      NamedEntityFilterPipe("PERSON")]),
                                                NamedEntityExtractorPipe()]),
                              SequencePipe([FindTokensPipe("VBD/**/*/pobj/NNP"),
                                                AggregatePipe([NamedEntityFilterPipe("LOC"), 
                                                      NamedEntityFilterPipe("PERSON")]),
                                                NamedEntityExtractorPipe()])])]

      _engine = PipelineEngine(_pipes_structure, Context(_doc), [0, 1, 2])
      _result = _engine.process()

      messagebox.showinfo("Resultado", _result)
      return

_btnexe = Button(_root, text="Ejecutar", command= run_program)
_btnexe.pack(side='right', fill='both', expand=True)

_root.mainloop()
