#!/usr/bin/env python3

import random
import spacy

TRAIN_DATA = [
    ("Uber blew through $1 million a week", {'entities': [(0, 4, 'ORG')]}),
    ("Google rebrands its business apps", {'entities': [(0, 6, "ORG")]})]

nlp = spacy.blank('en')
nlp.vocab.vectors.name = 'spacy_testing_training_vectors'
optimizer = nlp.begin_training()
for i in range(20):
    random.shuffle(TRAIN_DATA)
    for text, annotations in TRAIN_DATA:
        nlp.update([text], [annotations], sgd=optimizer)
nlp.to_disk('/Proyectos/TextMining/textminingapp/model')